using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Card : MonoBehaviour
{
    // Enum for card suits
    public enum Suit
    {
        Hearts,
        Diamonds,
        Clubs,
        Spades
    }

    // Enum for card ranks
    public enum Rank
    {
        Ace = 1, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King
    }

    // Variables to store suit and rank
    public Suit cardSuit;
    public Rank cardRank;
    public int cardValue;
    public string Cardcolor;
    public int parentIndexInArray;
    public bool isInFreeCell;
    public Transform cardFreeCell;
    public GameObject cardTableCell;
    public bool canAddToFoundationCells = false;
    public bool isInFoundationCell = false;
    public bool justRemovedfromFreeCell = false;

    RectTransform rt;
    public Vector3 worldPos;

    // Whether the card is draggable or not
    public bool isDraggable = false;


    private void OnEnable()
    {
        rt = this.GetComponent<RectTransform>();
    }

    // Function to set card properties
    public void SetCard(Sprite cardSprite, Suit newSuit, Rank newRank, int newvalue)
    {
        cardSuit = newSuit;
        cardRank = newRank;
        cardValue = newvalue;
        GetComponent<Image>().sprite = cardSprite;
        if (cardSuit == Suit.Clubs || cardSuit == Suit.Spades)
            Cardcolor = "Black";
        else
            Cardcolor = "Red";

        gameObject.name = newRank.ToString() + " of "+newSuit.ToString();
    }
    
    private void Update()
    {
        if (DragMovement.sequenceCard.Contains(this) && DragMovement.dragging)
        {
            int sequLIstInd = DragMovement.sequenceCard.IndexOf(this);
            Debug.Log(sequLIstInd);
            rt.position = (worldPos) - new Vector3(0,  sequLIstInd/(6f+sequLIstInd%8f), 0); 
        }
    }
}
