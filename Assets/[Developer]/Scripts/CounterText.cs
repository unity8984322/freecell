using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class CounterText : MonoBehaviour
{

    public void OnEnable()
    {
        StartCoroutine(CounterTxt());      
    }

    public IEnumerator CounterTxt()
    {
        TextMeshProUGUI text = GameManager.instance.counterTxt.GetComponentInChildren<TextMeshProUGUI>();
    //    GameManager.instance.OnClickPlay();

        yield return new WaitForSeconds(0.2f);
        text.text = "3";

        yield return new WaitForSeconds(1);
        text.text = "2";

        yield return new WaitForSeconds(1);
        text.text = "1";

        yield return new WaitForSeconds(1);

        text.text = "";
        GameManager.instance.counterTxt.SetActive(false);
        GameManager.instance.OnClickPlay();
    }
}
