using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;
using System;

public class DragMovement : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerExitHandler, IPointerClickHandler
{
    public static DragMovement instance;

    private RectTransform rectTransform;
    public static bool cardIsInList;
    public static bool dragging = false;
    public static GameObject freeCell;
    Card thisCard;
    private void OnEnable()
    {
        thisCard = GetComponent<Card>();
    }
    private void Start()
    {
        if (instance = null)
            instance = this;
    }

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        GameManager.instance.currentCard = null;
        GameManager.instance.cardBehind = null;
        sequenceCard = new();

        if (sequenceCard.Count == 0)
            GameManager.instance.currentCard = this.thisCard;
        else
            GameManager.instance.currentCard = sequenceCard[^1];

        if (!thisCard.isDraggable || thisCard.isInFoundationCell)
            return;

        if (Input.touchCount >1)
            return;        

        this.transform.parent.SetAsLastSibling();
        cardIsInList = GameManager.instance.tableCardStacks.SelectMany(stack => stack).Any(card => card == thisCard);
        Debug.Log(cardIsInList);

        bool inSequence = IsCardInSequence();
        if (inSequence)
            Debug.Log("cards are in sequence");
        else
        {
            sequenceCard.Add(this.thisCard);
            Debug.Log($"cards are not in sequence {sequenceCard[0]}");
        }
        if (!this.thisCard.isInFreeCell)
            GameManager.removeListElement?.Invoke(this.transform.parent);
    }

    public Transform behindObj = null;
    bool t = false;
    Card newC=null;

    public void OnDrag(PointerEventData eventData)
    {
        if (Input.touchCount > 1)        
            return;

        dragging = true;
        if (!thisCard.isDraggable || thisCard.isInFoundationCell)
            return;

        transform.GetChild(0).gameObject.SetActive(true);

        if (sequenceCard.Count >= 1)
        {
            float initialY = eventData.position.y;
            for (int i = 0; i < sequenceCard.Count; i++)
            {
                Card c = sequenceCard[i];
                RectTransformUtility.ScreenPointToWorldPointInRectangle(c.GetComponent<DragMovement>().rectTransform, eventData.position, eventData.pressEventCamera, out Vector3 worldPos);             
                c.worldPos = worldPos;
                c.transform.SetParent(GameManager.instance.freeStateParent);
                c.isDraggable = true;
            }
        }

        else
        {
            this.transform.SetParent(GameManager.instance.freeStateParent);
            RectTransformUtility.ScreenPointToWorldPointInRectangle(rectTransform, eventData.position, eventData.pressEventCamera, out Vector3 worldPos);
            rectTransform.position = worldPos;
        }
        behindObj = GetCardBehindPointer(eventData);
        
        if (behindObj != null)
        {
            if (behindObj.TryGetComponent(out Card card))
                GameManager.instance.cardBehind = card;
            else GameManager.instance.cardBehind = null;

            Debug.Log($"behind Obj: {behindObj.name}");
        }

        PointerEventData pointerData = new(EventSystem.current);
        pointerData.position = eventData.position;
        List<RaycastResult> results = new();
        EventSystem.current.RaycastAll(pointerData, results);

        foreach (var hitObject in results)
        {
            if (hitObject.gameObject.CompareTag("FreeCell") && hitObject.gameObject.transform.childCount == 0)
            {
                freeCell = hitObject.gameObject;
            }
        }
    }

    public static bool hasChildObjects = false;
    public void OnEndDrag(PointerEventData eventData)
    {
        dragging = false;
        Debug.Log("Runnnig onEndDrag()");

        if (!thisCard.isDraggable || thisCard.isInFoundationCell)
            return;

        if (behindObj != null)
        {
            Debug.Log("Behind obj != null" + behindObj.name);
            if (this.thisCard.canAddToFoundationCells)
            {
                Debug.Log("below block is if foundationCell condition not matched");
                if (!CheckFndCardCondition())
                {
                    Debug.Log("hovering over foundation but Foundation Condition not matched");
                    //if foundation Card condition not matched
                    if (!this.thisCard.isInFreeCell && !GameManager.instance.CompareTag("FoundationCell"))
                    {
                        Debug.Log("Card wasn't in free cell");
                        GameManager.AddListElement(true);
                    }
                    else
                    {
                        Debug.Log("Card was in free cell");
                        if (this.thisCard.isInFreeCell && sequenceCard.Count==1)
                        {
                            Debug.Log("Setting back parent - cardFreeCell");
                            SetTransform(thisCard.cardFreeCell);
                            thisCard.isInFreeCell = true;
                        }
                        else
                        {
                            Debug.Log("No free cell was available so getting back to previous list");
                            GameManager.AddListElement(true);
                        }
                    }
                    return;
                }
                Debug.Log("hovering over foundation but Foundation Condition matched");
                this.thisCard.isInFoundationCell = true;
                thisCard.isInFreeCell = false;
                SetTransform(behindObj);
                GameManager.instance.tableCardStacks[this.thisCard.parentIndexInArray].Remove(this.thisCard);
                if (GameManager.instance.tableCardStacks[thisCard.parentIndexInArray].Count >= 1)
                    GameManager.instance.tableCardStacks[this.thisCard.parentIndexInArray][^1].isDraggable = true;
                GameManager.AddScore();
                GameManager.instance.AddMoves();
                return;
            }

            if (GameManager.instance.cardBehind == null)
            {
                Debug.Log("Cardbehind == null but behind obj != null" + behindObj.name);
                if (behindObj.CompareTag("FoundationCell"))
                {
                    Debug.Log("hovering over the foundation cell");
                    if (CheckFndCardCondition())//|| (GameManager.instance.cardBehind.isInFoundationCell && CheckFndCardCondition())
                    {
                        Debug.Log("setting to Foundation card " + behindObj);
                        SetTransform(behindObj);
                        this.thisCard.isInFoundationCell = true;
                        this.thisCard.isInFreeCell = false;
                        GameManager.instance.tableCardStacks[this.thisCard.parentIndexInArray].Remove(this.thisCard);
                        if (GameManager.instance.tableCardStacks[this.thisCard.parentIndexInArray].Count >= 1)
                            GameManager.instance.tableCardStacks[this.thisCard.parentIndexInArray][^1].isDraggable = true;
                        GameManager.instance.AddMoves();
                        GameManager.AddScore();
                    }
                    else
                    {
                        Debug.Log("foundation condition did not match adding back to previous list");
                        if (thisCard.isInFreeCell)
                        {
                            SetTransform(thisCard.cardFreeCell);
                            return;
                        }
                        GameManager.AddListElement(true);
                    }
                    return;
                }
                else if (behindObj.CompareTag("FreeCell") || this.thisCard.cardFreeCell != null)
                {
                    if (sequenceCard.Count > 1)
                    {
                        GameManager.AddListElement(true);
                        return;
                    }
                    if (behindObj.CompareTag("TableCell"))
                    {
                        Debug.Log("Hovering on TableCell");
                        SetTransform(behindObj);
                        GameManager.instance.tableCardStacks[thisCard.parentIndexInArray].Add(thisCard);
                        Debug.Log(GameManager.instance.tableCardStacks[thisCard.parentIndexInArray].Count + "name: haha " + GameManager.instance.tableCardPositions[thisCard.parentIndexInArray]);
                        thisCard.isInFreeCell = false;
                        GameManager.instance.AddMoves();
                        return;   
                    }
                    SetTransform(thisCard.cardFreeCell);
                    thisCard.isInFreeCell = true;
                    Debug.Log("Hovering over the free Cell");
                    GameManager.instance.AddMoves();
                }
                else if (behindObj.CompareTag("TableCell"))
                {
                    Debug.Log("Hovering over the tabular Cell");
                    if (behindObj.CompareTag("TableCell"))
                        {
                        SetTransform(behindObj);
                        Debug.Log($"required index: {Array.IndexOf(GameManager.instance.tableCardPositions, behindObj)}");
                            this.thisCard.parentIndexInArray = Array.IndexOf(GameManager.instance.tableCardPositions, behindObj);
                            GameManager.AddListElement(false);
                        }
                }
                else
                {
                    Debug.Log("Card to sent to previous list");
                    GameManager.AddListElement(true);
                }
            }
            if (GameManager.instance.cardBehind != null)
            {
                Debug.Log("card behind != null");
                if (!GameManager.instance.cardBehind.isInFreeCell)
                {
                    Debug.Log("card was not in free cell");
                    if (behindObj.CompareTag("FoundationCell") || (GameManager.instance.cardBehind.isInFoundationCell && CheckFndCardCondition()))
                    {
                        if (sequenceCard.Count > 1)
                        {
                            GameManager.AddListElement(true);
                            return;
                        }
                        Debug.Log("setting to Foundation card " + behindObj.name);
                        SetTransform(GameManager.instance.cardBehind.transform.parent);
                        thisCard.isInFreeCell = false;
                        thisCard.isInFoundationCell = true;
                        //Debug.Log("\t\t yeh wala part" + thisCard.cardTableCell.GetComponent<TableCell>().stackIndex);
                        if (GameManager.instance.tableCardStacks[this.thisCard.parentIndexInArray].Count >= 1)
                        {
                            GameManager.instance.tableCardStacks[this.thisCard.parentIndexInArray].Remove(this.thisCard);
                            GameManager.instance.tableCardStacks[this.thisCard.parentIndexInArray][^1].isDraggable = true;
                        }
                        GameManager.instance.AddMoves();
                        GameManager.AddScore();
                        return;
                    }
                    if (this.thisCard.isInFreeCell && GameManager.instance.cardBehind!=null && !IsCardCompatible())
                    {
                        Debug.Log("cardWas in free Cell and cardbehind is not null");
                        SetTransform(thisCard.cardFreeCell);
                        thisCard.isInFreeCell = true;
                        return;
                    }
                    if(IsCardCompatible())
                    {
                        Debug.Log("Card is compatible");
                        if(!thisCard.isInFreeCell)
                        {
                            if (sequenceCard.Count > 0)
                            {
                                GameManager.AddListElement(false);
                                return;
                            }
                            Debug.Log("Card was in free cell");
                            SetTransform(thisCard.cardFreeCell);
                            return;
                        }
                        Debug.Log("Behind card is compatible with this card");
                        GameManager.AddListElement(false);
                        return; 
                    }
                    if (!IsCardCompatible(null, this.thisCard))
                    {
                        Debug.Log("Cards are of same color or not compatible so return to previous position");
                        GameManager.AddListElement?.Invoke(true);
                        return;
                    }
                   else if(behindObj.CompareTag("FoundationCell") || (GameManager.instance.cardBehind.isInFoundationCell && !CheckFndCardCondition()))
                    {
                        Debug.Log("Card behind is in free cell");
                        if (thisCard.isInFreeCell)
                        {
                            Debug.Log("thisCard is in freecell;");
                            SetTransform(thisCard.cardFreeCell);
                            return;
                        }
                        Debug.Log("Cards are of different color & compitable so add to new list");
                        GameManager.AddListElement(true);
                        return;
                    }

                }
                else
                {
                    if (sequenceCard.Count > 1)
                    {
                        GameManager.AddListElement(true);
                        return;
                    }

                    Debug.Log("behind card is in the freecell" + GameManager.instance.cardBehind + "\t" +behindObj);
                    if (IsCardCompatible(null, null))
                    {
                        if (thisCard.isInFreeCell)
                        {
                            SetTransform(thisCard.cardFreeCell);
                        return;
                        }
                        Debug.Log("cards are compatible and colors aren't same so add to previous list but behind card is in free cell");
                        GameManager.AddListElement(true);
                    }
                    else if (this.thisCard.isInFreeCell)
                    {
                        Debug.Log("return cards to free cell");
                        SetTransform(thisCard.cardFreeCell);
                        thisCard.isInFreeCell = true;
                        GameManager.instance.AddMoves();
                        Debug.Log("Added moves");
                    }
                    else
                        GameManager.AddListElement(true);
                }
            }

        }
        else        //behind obj is null
        {
            if (GameManager.instance.cardBehind != null)
            {
                Debug.Log("Behind Card != null");
                if(this.thisCard.isInFreeCell)
                {
                    SetTransform(thisCard.cardFreeCell);
                    thisCard.isInFreeCell = true;
                    GameManager.instance.AddMoves();
                }
                else
                {
                    if (this.thisCard != null && IsCardCompatible(null, null))
                    {
                        GameManager.AddListElement(false);
                        return;
                    }
                    Debug.Log("Adding to previous list");
                    GameManager.AddListElement(true);
                }
            }
            else        //card behind is null
            {
                if (thisCard.isInFreeCell)
                {
                    SetTransform(thisCard.cardFreeCell);
                    thisCard.isInFreeCell = true;
                }
                else
                {
                    if (this.thisCard.cardTableCell != null)
                    {
                        GameManager.AddListElement(false);
                        return;
                    }
                    Debug.Log("not in free cell & Adding to previous list");
                    GameManager.AddListElement(true);
                }
                Debug.Log("Behind Card == null");
            }
            if (thisCard.cardFreeCell != null)//&& behindObj == null) //&& cardIsInList 
            {
                if (sequenceCard.Contains(this.thisCard) && sequenceCard.Count > 1)
                {
                    Debug.Log("This block is to add card to previous list");
                    GameManager.AddListElement(true);
                    return;
                }
                Debug.Log("This block is for add to cardFreeCell");
                if (thisCard.cardFreeCell != null)
                { 
                    if(this.thisCard.cardTableCell!=null)
                    {
                        Debug.Log("cardTableCell != null");
                        SetTransform(thisCard.cardTableCell.transform);
                        GameManager.AddListElement(false);
                        cardIsInList = true;
                        if (GameManager.instance.tableCardStacks[this.thisCard.parentIndexInArray].Count >= 1)
                            GameManager.instance.tableCardStacks[this.thisCard.parentIndexInArray][^1].isDraggable = true;
                        GameManager.instance.AddMoves();
                        return;
                    }    

                    SetTransform(thisCard.cardFreeCell);
                    thisCard.isInFreeCell = true;
                    if (GameManager.instance.tableCardStacks[this.thisCard.parentIndexInArray].Count >= 1)                   
                        GameManager.instance.tableCardStacks[this.thisCard.parentIndexInArray][^1].isDraggable = true;
                    
                    cardIsInList = false;
                        GameManager.instance.AddMoves();
                }
                else
                {
                    GameManager.AddListElement(true);
                GameManager.instance.tableCardStacks[this.thisCard.parentIndexInArray][^1].isDraggable = true;
                }
                return;
            }
        }
        dragging = false;
    }

    public void SetTransform(Transform t)
    {
        if((!thisCard.isInFoundationCell || !thisCard.isInFreeCell) && GameManager.instance.tableCardStacks[thisCard.parentIndexInArray].Count >0)
            GameManager.instance.tableCardStacks[thisCard.parentIndexInArray][^1].GetComponent<Image>().color = Color.white;

        if (behindObj !=null && behindObj.CompareTag("TableCell") && sequenceCard.Count >1)
        {
            foreach (Card c in sequenceCard)
            {
                c.cardTableCell = behindObj.gameObject;
                c.transform.parent = t;
                c.transform.localPosition = t.transform.position;
                c.parentIndexInArray = Array.IndexOf(GameManager.instance.tableCardPositions, c.transform.parent);
            }
            return;
        }
        thisCard.transform.SetParent(t);
        thisCard.transform.localPosition = t.transform.position;        
    }

    bool CheckFndCardCondition()
    {
        if (behindObj.TryGetComponent(out Card tempCard))
        {
            Debug.Log($"Behind card: { tempCard}");
            if (FndCardDiff(tempCard, thisCard))
                return true;
        }
        else if (tempCard == null && behindObj.CompareTag("FoundationCell") && thisCard.cardValue == 1)
            return true;
        return false;

    }

    bool FndCardDiff(Card behindCard, Card currentCard)
    {
        if (behindCard.Cardcolor != currentCard.Cardcolor || behindCard.cardSuit != currentCard.cardSuit)
            return false;

        if (Mathf.Abs(behindCard.cardValue - currentCard.cardValue) == 1 && behindCard.cardValue < currentCard.cardValue)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static List<Card> sequenceCard = new();

    public bool IsCardInSequence()
    {
        int SeqCardcount = 0;
        if (!GameManager.instance.tableCardStacks.Any(stack => stack.Contains(thisCard)))
            return false;

        foreach (var stack in GameManager.instance.tableCardStacks)
        {
            int index = stack.IndexOf(thisCard);
            if (index != -1 && index != stack.Count)
            {
                for (int i = index + 1; i < stack.Count; i++)
                {
                    Debug.Log($"card to compare: behindCard {stack[i - 1]} currentCard {stack[i]}");
                    if (IsCardCompatible(stack[i - 1], stack[i]))
                    {
                        sequenceCard.Add(stack[i - 1]);
                        sequenceCard.Add(stack[i]);
                        Debug.Log($"Card Added to list: {stack[i]}\t {stack[i - 1]}");
                        SeqCardcount++;
                    }
                }
            }
        }
        if (SeqCardcount >= 1)
            return true;
        else
            return false;

    }

    public bool IsCardCompatible(Card behindCard = null, Card currentCard=null)
    {
        behindCard = behindCard == null ? GameManager.instance.cardBehind : behindCard;
        currentCard = currentCard == null ? this.thisCard :  currentCard;
        if (behindCard.Cardcolor == currentCard.Cardcolor)
            return false;

        if (Mathf.Abs(behindCard.cardValue - currentCard.cardValue) == 1 && behindCard.cardValue > currentCard.cardValue)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GameManager.instance.cardBehind = null;
    }

    [SerializeField] GameObject[] behindObjects;
    private static Transform GetCardBehindPointer(PointerEventData eventData)
    {
        Card resultantCard = null;
        PointerEventData pointerData = new(EventSystem.current);
        pointerData.position = eventData.position;
        List<RaycastResult> results = new();
        EventSystem.current.RaycastAll(pointerData, results);

        foreach (var hit in results)
        {
            Debug.Log("<color=Yellow>behind object = :</color>" + hit);

            if(hit.gameObject.CompareTag("TableCell"))            
                GameManager.instance.currentCard.cardTableCell = hit.gameObject;            
            if (hit.gameObject.CompareTag("FreeCell"))
                GameManager.instance.currentCard.cardFreeCell = hit.gameObject.transform;

            Card card = hit.gameObject.TryGetComponent(out Card c) ? c : null;
            
            if (card != null && !sequenceCard.Contains(card)) //&& card != this.thisCard 
            {
                if (card == GameManager.instance.currentCard)
                    return null;

                Card lastCard = card.transform.parent.GetChild(card.transform.parent.childCount - 1).GetComponent<Card>();
                resultantCard = lastCard;
                return resultantCard.transform;
            }
            else if(card ==null)            
                return hit.gameObject.transform;            
        }
        return null;
    }

    public Transform tempTransform = null;
    public int indexForNewLIst = 0;
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.dragging)
            return;

        if ((GameManager.instance.tableCardStacks[thisCard.parentIndexInArray].Contains(thisCard) && (thisCard != GameManager.instance.tableCardStacks[thisCard.parentIndexInArray][^1]))|| thisCard.isInFoundationCell || dragging)
            return;

        Debug.Log("the code should run fine");
        Transform emptyFreeCell = GameManager.instance.freeCellPositions.FirstOrDefault(t => t.childCount == 0);
        Transform emptyTableCell = GameManager.instance.tableCardPositions.FirstOrDefault(t => t.childCount == 0);
        indexForNewLIst = 0;
        GameManager.instance.currentCard = thisCard;
        if (CheckFoundationOnCardClick(thisCard))
        {
            Debug.Log("Card set in foundation Cell");
            if (!thisCard.isInFreeCell)
                GameManager.instance.tableCardStacks[thisCard.parentIndexInArray].Remove(thisCard);
            thisCard.isInFoundationCell = true;
            thisCard.isInFreeCell = false;
            SetTransform(tempTransform);
            GameManager.instance.AddMoves();
            GameManager.AddScore();
        }
        else if (CheckCardCompatibilityOnCardClick(thisCard))
        {
            if (!thisCard.isInFreeCell)
                GameManager.instance.tableCardStacks[thisCard.parentIndexInArray].Remove(thisCard);
            Debug.Log("Card set in table Cell Cell + " + this.transform.parent);
            thisCard.isInFreeCell = false;
            GameManager.instance.tableCardStacks[thisCard.parentIndexInArray].Remove(thisCard);
            GameManager.instance.tableCardStacks[indexForNewLIst].Add(thisCard);
            thisCard.parentIndexInArray = indexForNewLIst;
            SetTransform(GameManager.instance.tableCardPositions[indexForNewLIst]);
            GameManager.instance.AddMoves();
        }
        else if (thisCard.isInFreeCell)
        {
            Debug.Log("Clicked on free cell card");
            if (CheckCardCompatibilityOnCardClick(thisCard))
            {
                Debug.Log("card was in free cell so returning back in table cell");
                GameManager.instance.tableCardStacks[indexForNewLIst].Add(thisCard);
                thisCard.parentIndexInArray = indexForNewLIst;
                SetTransform(GameManager.instance.tableCardPositions[indexForNewLIst]);
                thisCard.isInFreeCell = false;
                GameManager.instance.AddMoves();
            }
            else if(GameManager.instance.tableCardPositions.Any(t => t.childCount == 0))
            {
                Debug.Log("moving to free table cell");
                thisCard.cardTableCell = emptyTableCell.gameObject;
                thisCard.parentIndexInArray = Array.IndexOf(GameManager.instance.tableCardPositions, emptyTableCell);
                GameManager.instance.tableCardStacks[thisCard.parentIndexInArray].Add(thisCard);
                SetTransform(emptyTableCell);
                thisCard.isInFreeCell = false;
                GameManager.instance.AddMoves();
            }
        }
        else if (GameManager.instance.freeCellPositions.Any(t => t.childCount == 0))
        {
            Debug.Log($"Card set in free Cell {GameManager.instance.tableCardStacks[thisCard.parentIndexInArray].Count} \t {thisCard.name}" +
                $"\t remove from {GameManager.instance.tableCardPositions[thisCard.parentIndexInArray]}");
            GameManager.instance.tableCardStacks[thisCard.parentIndexInArray].Remove(thisCard);
            this.thisCard.isInFreeCell = true;
            thisCard.cardFreeCell = emptyFreeCell;
            SetTransform(emptyFreeCell);

            GameManager.instance.AddMoves();
        }
        else if (GameManager.instance.tableCardPositions.Any(t => t.childCount == 0))
        {
            GameManager.instance.tableCardStacks[thisCard.parentIndexInArray].Remove(thisCard);
            thisCard.cardTableCell = emptyTableCell.gameObject;
            SetTransform(emptyTableCell);
            thisCard.parentIndexInArray = Array.IndexOf(GameManager.instance.tableCardPositions, emptyTableCell);
            GameManager.instance.tableCardStacks[thisCard.parentIndexInArray].Add(thisCard);
            GameManager.instance.AddMoves();
            dragging = false;
            return;
        }
        else        
            Debug.Log("No possible Move available"); 
        
        dragging = false;
        Debug.Log("This card is in free cell" + GameManager.instance.currentCard.isInFreeCell);
    }

        public bool CheckFoundationOnCardClick(Card clickedCard)
    {
        foreach (var foundationCell in GameManager.instance.foundationPositions)
        {
            if (foundationCell.childCount <= 0 && clickedCard.cardValue == 1)
            {
                tempTransform = foundationCell;
                return true;
            }
            else if (foundationCell.childCount > 0)
            {
                Card topCard = foundationCell.GetChild(foundationCell.childCount-1).GetComponent<Card>();
                if (topCard.cardSuit == clickedCard.cardSuit && (int)topCard.cardValue + 1 == (int)clickedCard.cardValue)
                {
                    tempTransform = foundationCell;
                    return true;
                }
            }
        }
        return false;
    }

    public bool CheckCardCompatibilityOnCardClick(Card currentCard)
    {
        foreach (var stack in GameManager.instance.tableCardStacks)
        {
            if (stack.Count > 0)//&& stack!= GameManager.instance.tableCardStacks[currentCard.parentIndexInArray]
            {                
                Card lastCard = stack[^1]; // Get the last card in the stack
                Debug.Log($"Last Card: {lastCard} : {lastCard.cardValue} \t CurrentCard: {currentCard} : {currentCard.cardValue}");
                if (lastCard.Cardcolor == currentCard.Cardcolor)
                    continue;
                else if (Mathf.Abs(lastCard.cardValue - currentCard.cardValue) == 1 && lastCard.cardValue > currentCard.cardValue)
                {
                    indexForNewLIst = lastCard.parentIndexInArray;
                    return true;
                }
            }
        }
        return false;
    }
}
