using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FoundationCells : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (GameManager.instance.currentCard != null)
        {

        Debug.Log("Entered the foundation cell");
            GameManager.instance.currentCard.canAddToFoundationCells = true;
        }
    }

    
    public void OnPointerExit(PointerEventData eventData)
    {
        if (GameManager.instance.currentCard != null)
        {

        Debug.Log("Exitted the foundation cell");
        GameManager.instance.currentCard.canAddToFoundationCells = false;
        }
    }
}

