using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FreeCell : MonoBehaviour, IPointerEnterHandler
{
    [SerializeField] GameObject enteredObject;
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (transform.childCount != 0)
            return;
        if (GameManager.instance.currentCard != null && DragMovement.dragging)
        {
            Debug.Log("Hey there");
            GameManager.instance.currentCard.isInFreeCell = true;
        }
        enteredObject = eventData.pointerEnter.gameObject;
    }
}
