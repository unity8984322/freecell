using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class GameComplete : MonoBehaviour
{

    public static System.Action<bool,int, int, string> OnGameComplete;

    [SerializeField] TextMeshProUGUI resultTxt;
    [SerializeField] TextMeshProUGUI pScore;
    [SerializeField] TextMeshProUGUI timeTaken;
    [SerializeField] TextMeshProUGUI pmoves;

    private void OnEnable()
    {
        OnGameComplete += ShowResult;
        GameManager.isGameComplete= true;
    }

    private void OnDisable()
    {
        OnGameComplete -= ShowResult;
    }

    private void ShowResult(bool result ,int score, int moves, string time)
    {
        if (result)
        {
            resultTxt.color = Color.green;
            resultTxt.text = " Congratulations You've won!!!";
        }
        else
        {
            resultTxt.color = Color.red;
            resultTxt.text = "Better Luck Next Time";
        }

        pScore.text = "Score:  " + score.ToString();
        timeTaken.text ="Time:  " + time.ToString();
        pmoves.text = "Moves: " + moves.ToString();

    }
}
