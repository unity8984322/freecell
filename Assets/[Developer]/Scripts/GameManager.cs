using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using DG.Tweening;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public static System.Action<Transform> removeListElement;
    public static System.Action<bool> AddListElement;
    public static System.Action CheckFoundationStack;
    [SerializeField] Color greyColor;
    [SerializeField] int usedTimeinSeconds = 0;
    [SerializeField] TextMeshProUGUI timerText;
    [SerializeField] int intScore = 0;
    [SerializeField] TextMeshProUGUI score;
    [SerializeField] int intMoves = 0;
    public static bool isGameComplete = false;
    public TextMeshProUGUI moves;
    public GameObject blockImage;
    public GameObject counterTxt;
    [SerializeField] GameObject cPrefab;
    [SerializeField] Transform parent;
    public Transform freeStateParent;
    public Transform[] tableCardPositions; // Positions for the tableau stacks
    public Transform[] foundationPositions; // Positions for the foundation stacks
    public Transform[] freeCellPositions; // Positions for the free cells

    [SerializeField] List<Sprite> cardSprites;
    [SerializeField] List<Card> deck = new();

    public List<Card>[] tableCardStacks = new List<Card>[8]; // Tableau stacks

    [Header("Current Card Set")]
    public Card currentCard;
    public Card cardBehind;

    void Start()
    {
        if (instance == null)
            instance = this;
    }

    private void OnEnable()
    {
        removeListElement += OnListRemoveAction;
        AddListElement += OnListAddAction;
    }

    private void OnDisable()
    {
        removeListElement -= OnListRemoveAction;
        AddListElement -= OnListAddAction;
    }

    int count = 0;

    public void OnClickPlay()
    {
        StartCoroutine(CreateDeck());
    }

    // Function to create a deck of cards
    IEnumerator CreateDeck()
    {
        int count = 0;
        for (int s = 0; s < 4; s++)
        {
            for (int r = 1; r <= 13; r++)
            {
                Card newCard = Instantiate(cPrefab, Vector3.zero, Quaternion.identity, parent).GetComponent<Card>();
                newCard.SetCard(cardSprites[count], (Card.Suit)s, (Card.Rank)r, r);
                deck.Add(newCard);
                count++;
                yield return new WaitForSeconds(0.01f);
            }
        }

        ShuffleDeck1();
    }

    // Function to shuffle the deck
    void ShuffleDeck1()
    {
        for (int i = 0; i < deck.Count; i++)
        {
            Card temp = deck[i];
            int randomIndex = Random.Range(i, deck.Count);
            deck[i] = deck[randomIndex];
            deck[randomIndex] = temp;
        }

        StartCoroutine(DealCards(MainMenu.level));
    }

    // Function to deal cards to tableau, foundation, and free cells
    IEnumerator DealCards(Level difficulty)
    {
        blockImage.SetActive(true);
        int cardIndex = 0;
        yield return new WaitForSeconds(0.3f);

        // Shuffle the deck based on difficulty level
        
        if (difficulty == Level.Medium)
        {
            ShuffleDeck();
        }
        else if (difficulty == Level.Hard)
        {
            ShuffleDeckToMakeItDifficult();
        }

        for (int i = 0; i < 8; i++)
        {
            tableCardStacks[i] = new List<Card>();   // Initialize each stack
            for (int j = 0; j < Random.Range(2, 13); j++)
            {
                if (cardIndex == 52)
                    break;

                Card card = deck[cardIndex++];
                tableCardStacks[i].Add(card);
                card.transform.SetParent(tableCardPositions[i]);
            }

            tableCardPositions[i].GetComponent<TableCell>().SetData(i);
            tableCardPositions[i].GetComponent<VerticalLayoutGroup>().spacing = -145;
        }

        while (cardIndex < deck.Count)
        {
            int randomStack = Random.Range(0, 8); // Choose a random stack
            List<Card> minCountList = tableCardStacks.Where(stack => stack.Count >= Random.Range(5, 7)).OrderBy(stack => stack.Count).FirstOrDefault();
            int index = System.Array.IndexOf(tableCardStacks, minCountList);
            Transform parent = tableCardPositions[index];
            Card card = deck[cardIndex];
            minCountList.Add(card);
            card.transform.SetParent(parent);
            cardIndex++;
        }

        List<int> tempIntList = new() { 1, 3, 5, 7, 2, 4, 6 };
        for (int i = 0; i < 8; i++)
        {
            List<Card> l = tableCardStacks[i];
            if (MainMenu.level != Level.Easy)
            {
                foreach (Card c in l)
                {
                    c.parentIndexInArray = i;
                    c.GetComponent<Image>().color = greyColor;
                }
            }
            else
            {
                // Sort cards within the stack
                for (int j = 0; j < l.Count - 1; j++)
                {
                    for (int k = j + 1; k < l.Count; k++)
                    {
                        //if (tempIntList[Random.Range(0, tempIntList.Count)] % 2 == 0)
                        {
                            if (l[j].cardValue < l[k].cardValue)
                            {
                                if (tempIntList[Random.Range(0, tempIntList.Count)] % 2 == 0)
                                    continue;
                                // Swap cards
                                var temp = l[j];
                                l[j] = l[k];
                                l[k] = temp;
                            }
                        }
                    }
                }
            }

            Debug.Log("Array: " + tableCardPositions[i] + " LastCard: " + tableCardStacks[i][^1].name);
            for (int j = 0; j < tableCardStacks[i].Count; j++)
            {
                Card card = tableCardStacks[i][j];
                card.parentIndexInArray = i;
                card.GetComponent<Image>().color = greyColor;
                card.transform.SetSiblingIndex(j);
            }
            l[^1].isDraggable = true;
        }

        InvokeRepeating(nameof(StartTimer), 1, 1);
        ProcessAllCardsInSequence();
        HighlightAllCardsInSequence();
        blockImage.SetActive(false);
    }

    void ShuffleDeck()
    {
        deck = deck.OrderBy(card => System.Guid.NewGuid()).ToList();
    }

    void StartTimer()
    {
        usedTimeinSeconds++;
        var ts = System.TimeSpan.FromSeconds(usedTimeinSeconds);
        timerText.text = string.Format("{00:00}:{01:00}", (int)ts.TotalMinutes, (int)ts.Seconds);
    }

    void ShuffleDeckToMakeItDifficult()
    {
        deck = deck.OrderBy(card => System.Guid.NewGuid()).ToList();
        for (int i = 1; i < deck.Count; i++)
        {
            if (CanAddToPrevious(deck[i], deck[i - 1]))
            {
                int randomIndex = UnityEngine.Random.Range(0, deck.Count);
                Card temp = deck[i];
                deck[i] = deck[randomIndex];
                deck[randomIndex] = temp;
            }
        }
    }

    bool CanAddToPrevious(Card current, Card previous)
    {
        return (current.Cardcolor != previous.Cardcolor && Mathf.Abs(current.cardValue - previous.cardValue) == 1);
    }

    List<Card> tempCardsList = new();
    public void ProcessAllCardsInSequence()
    {
        //This function isn't working
        foreach (List<Card> cLists in GameManager.instance.tableCardStacks)
        {
            List<Card> sequenceCard = new List<Card>(); // List to hold cards in sequence
            tempCardsList = cLists.ToList(); // Reverse the current list
            tempCardsList.Reverse();
            for (int i = 0; i < tempCardsList.Count; i++)
            {
                Card c = tempCardsList[i];
                int SeqCardcount = 0;
                sequenceCard.Clear();

                if (i != tempCardsList.Count - 1)
                {
                    for (int j = i + 1; j < tempCardsList.Count; j++)
                    {
                        if (checkCompaatibility(tempCardsList[j - 1], tempCardsList[j]))
                        {
                            sequenceCard.Add(tempCardsList[j - 1]);
                            sequenceCard.Add(tempCardsList[j]);
                            SeqCardcount++;
                        }
                        else
                        {
                            i = j - 1;
                            break;
                        }
                    }
                }

                foreach (Card tempC in tempCardsList)
                {
                    tempC.isDraggable = sequenceCard.Contains(tempC);
                    Debug.Log($"{tempC} is in sequence");

                }

                tempCardsList[0].isDraggable = true;
                tempCardsList[0].GetComponent<Image>().color = Color.white;
                tempCardsList[0].transform.GetChild(0).gameObject.SetActive(true);
            }
        }
    }

    [SerializeField] List<List<Card>> listOfListOfSequenceCards = new();

    public void HighlightAllCardsInSequence()
    {
        //Use this function to Check the possible hint moves in the GiveHint() this function need to be modified as it's not detecting sequence cards properly
        bool canBreakLoop = false ;
        Debug.Log("Looping through each list");
        foreach (List<Card> cLists in GameManager.instance.tableCardStacks)
        {
            canBreakLoop = false;
            List<Card> sequenceCard = new List<Card>(); // List to hold cards in sequence
            tempCardsList = cLists.ToList(); // Copy the current list
            //tempCardsList.Reverse(); // Reverse the list to process from bottom to top

            for (int i = tempCardsList.Count - 1; i >= 0; i--)
            {
                Debug.Log($"i: {i}");
                Card currentCard = tempCardsList[i];

                // Clear the sequenceCard list for each card
                sequenceCard.Clear();

                for (int j = i-1; j >= 0; j--)
                {
                    Debug.Log($"i: {i}\tj: {j}");
                    Card previousCard = tempCardsList[j];
                    Debug.Log($"PreviousCard:{previousCard}\t CurrentCard: {currentCard}");
                    if (checkCompaatibility(previousCard, currentCard))
                    {
                        if(!sequenceCard.Contains(previousCard))
                            sequenceCard.Add(previousCard);
                        if (!sequenceCard.Contains(currentCard))
                            sequenceCard.Add(currentCard);
                        currentCard.GetComponent<Image>().color = Color.white; // Highlight current card
                        previousCard.GetComponent<Image>().color = Color.white; // Highlight previous card
                        Debug.Log($"Sequence.Last: {sequenceCard.Last()} \t Sequence.First: {sequenceCard.First()}");
                    }
                    else
                    {
                        Debug.Log("Breakign he loop");
                        canBreakLoop = true;     
                        break; // If not in sequence, break the loop
                    }
                }

    //            tempCardsList[^1].isDraggable = true;
  //              tempCardsList[^1].GetComponent<Image>().color = Color.white;
//                tempCardsList[^1].transform.GetChild(0).gameObject.SetActive(true);
                listOfListOfSequenceCards.Add(sequenceCard);

                if (canBreakLoop)
                    break;
            }
        }

        foreach (List<Card> x in listOfListOfSequenceCards)
            foreach (Card c in x)
                Debug.Log($"{tableCardPositions[c.parentIndexInArray]} contains: {c}");
    }


    public bool checkCompaatibility(Card behindCard, Card currentCard)
    {
        if ((Mathf.Abs(behindCard.cardValue - currentCard.cardValue) == 1) && (behindCard.cardValue > currentCard.cardValue) &&
            behindCard.Cardcolor != currentCard.Cardcolor)        
            return true;        
        else        
            return false;        
    }


    #region ActionCalls

    void OnListRemoveAction(Transform stackToAdjust)
    {
        int requiredIndex = System.Array.IndexOf(tableCardPositions, stackToAdjust);
        Debug.Log($"required Index of item: {requiredIndex} \t stack to be removed from{stackToAdjust}");
        if (DragMovement.sequenceCard.Count <= 0)
            tableCardStacks[requiredIndex].Remove(currentCard);
        else
            foreach (Card c in DragMovement.sequenceCard)
            {
                {
                    Debug.Log("sequence card\t" + c.name);
                    tableCardStacks[requiredIndex].Remove(c);
                    Debug.Log($"{c} removed from {requiredIndex}" + tableCardStacks[currentCard.parentIndexInArray][^1]);
                }
            }
    }

    void OnListAddAction(bool addToPreviousList)
    {
        if (addToPreviousList)
        {
            Debug.Log($"Add to previous list");
            {
                Debug.Log("Getting required index and adding to previous list");
                int requiredIndex = 0;
                foreach (Card c in DragMovement.sequenceCard)
                {
                    c.isInFreeCell = false;
                    c.transform.SetParent(tableCardPositions[currentCard.parentIndexInArray]);
                    requiredIndex = System.Array.IndexOf(tableCardPositions, c.transform.parent);
                    if (tableCardStacks[requiredIndex].Contains(c))
                        continue;
                    tableCardStacks[requiredIndex].Add(c);

                }
                Debug.Log($"<color=Green>Required Index: {requiredIndex} \t transformName: {currentCard.transform.parent}</color>");
                Debug.Log($"Item Added: {currentCard} to \t {tableCardPositions[requiredIndex]}");
                tableCardStacks[currentCard.parentIndexInArray][^1].isDraggable = true;
            }
        }
        else
        {
            int requiredIndex = 0;

            Debug.Log($"Add to new list");
            AddMoves();
            foreach (Card c in DragMovement.sequenceCard)
            {
                if (cardBehind == null)
                {
                    foreach (Card sqCrd in DragMovement.sequenceCard)
                    {

                        sqCrd.transform.SetParent(currentCard.cardTableCell.transform);
                        sqCrd.transform.localPosition = currentCard.cardTableCell.transform.position;
                        sqCrd.parentIndexInArray = sqCrd.cardTableCell.GetComponent<TableCell>().stackIndex;
                        Debug.Log("------------" + sqCrd.cardTableCell + sqCrd.parentIndexInArray);
                        sqCrd.isDraggable = true;
                        if (tableCardStacks[sqCrd.parentIndexInArray].Contains(sqCrd))
                            continue;
                        tableCardStacks[sqCrd.parentIndexInArray].Add(sqCrd);

                    }
                    return;
                }

                requiredIndex = System.Array.IndexOf(tableCardPositions, cardBehind.transform.parent);
                c.isInFreeCell = false;
                c.parentIndexInArray = cardBehind.parentIndexInArray;
                c.transform.SetParent(tableCardPositions[cardBehind.parentIndexInArray]);
                tableCardStacks[requiredIndex].Add(c);
            }

            tableCardStacks[currentCard.parentIndexInArray][^1].isDraggable = false;
            tableCardStacks[cardBehind.parentIndexInArray][^1].isDraggable = true;
            Debug.Log($"<color=Green>Required Index: {requiredIndex} \t transformName: {cardBehind.transform.parent}</color>");
            Debug.Log($"Item Added: {currentCard} to \t {tableCardPositions[requiredIndex]}");
        }
        currentCard.cardFreeCell = null;
    }

    #endregion

    public static void AddScore()
    {
        instance.intScore += 10;
        instance.score.text = "Score:  " + instance.intScore;
        if (instance.intScore == 520)
        {
            UIManager.instance.ShowScreen(UIScreens_ENM.resultScreen, false);
            GameComplete.OnGameComplete?.Invoke(true, instance.intScore, instance.intMoves, instance.timerText.text);
            return;
        }
    }

    public void SetLastCardToDraggable()
    {
        foreach (var stack in instance.tableCardStacks)
        {
            if (stack.Count > 0)
            {
                Debug.Log($"<color=Yellow> {tableCardStacks[System.Array.IndexOf(tableCardStacks, stack)].Count}\t\t{tableCardPositions[System.Array.IndexOf(tableCardStacks, stack)]}" +
                    $"\t{string.Join(" \t ", stack.Select(x => x.ToString()).ToArray())}</color>");
                stack.Last().isDraggable = true;
                stack.Last().GetComponent<Image>().color = Color.white;
                stack.Last().transform.GetChild(0).gameObject.SetActive(true);
            }
        }
        blockImage.SetActive(false);
    }

    int tempCount = 0;
    public void AddMoves()
    {
        CancelInvoke(nameof(GiveHint));

        blockImage.SetActive(true);
        instance.intMoves += 1;
        instance.moves.text = "Moves: " + instance.intMoves;
        Invoke(nameof(SetLastCardToDraggable), 0.1f);

        Invoke(nameof(GiveHint),7);
    }

    void GiveHint()
    {
        HighlightAllCardsInSequence();

        bool canBreakLoop=false;
        Debug.Log("Do something idiot");

        foreach (var lst1 in tableCardStacks)
        {
            foreach (var lst2 in tableCardStacks.Where(lst => lst != lst1)) // Ensure lst2 is different from lst1
            {
                var foundationHintCards = (from card1 in deck
                                           from card2 in lst2
                                           where Mathf.Abs(card1.cardValue - card2.cardValue) == 1
                                                 && card1.cardSuit == card2.cardSuit
                                                 && card2.isDraggable
                                                 && card1.isInFoundationCell
                                           select System.Tuple.Create(card1, card2))
                                     .FirstOrDefault();

                var tableCellHintCards = (from card1 in lst1
                                          from card2 in lst2
                                          where Mathf.Abs(card1.cardValue - card2.cardValue) == 1
                                                && card1.Cardcolor != card2.Cardcolor
                                                && card1.isDraggable
                                                && card2.isDraggable
                                          select System.Tuple.Create(card1, card2))
                                     .FirstOrDefault();

                var freeCellHintCards = (from card1 in deck
                                         from card2 in lst2
                                         where Mathf.Abs(card1.cardValue - card2.cardValue) == 1
                                               && card1.cardSuit == card2.cardSuit
                                               && card1.isDraggable
                                               && card2.isDraggable
                                               && card1.isInFreeCell
                                         select System.Tuple.Create(card1, card2))
                                     .FirstOrDefault();

                if (foundationHintCards != null)
                {
                    canBreakLoop = true;
                 //   Debug.Log($"Matching Cards: {foundationHintCards.Item1}\t {foundationHintCards.Item2}");
                   // Animate(foundationHintCards.Item1.transform);
                    //Animate(foundationHintCards.Item2.transform);
                    //break;
                }
                else if (tableCellHintCards != null)
                {
                    canBreakLoop = true;
                    //Debug.Log($"Matching Cards: {tableCellHintCards.Item1}\t {tableCellHintCards.Item2}");
                    //Animate(tableCellHintCards.Item1.transform);
                    //Animate(tableCellHintCards.Item2.transform);
                    //break;
                }
                else if (freeCellHintCards != null)
                {
                    canBreakLoop = true;
                    //Debug.Log($"Matching Cards: {freeCellHintCards.Item1}\t {freeCellHintCards.Item2}");
                    //Animate(freeCellHintCards.Item1.transform);
                    //Animate(freeCellHintCards.Item2.transform);
                    //break;
                }
                else if(listOfListOfSequenceCards.Count>0)
                {
                    
                foreach (List<Card> t in listOfListOfSequenceCards)
                {
                        Debug.Log("Here it is");
                var sequenceHintCards = (from card1 in t
                                         from card2 in lst2.Where(element=>element=lst2[^1])
                                         where Mathf.Abs(card1.cardValue - card2.cardValue) == 1
                                               && card1.cardValue>card2.cardValue
                                               && card1.Cardcolor != card2.Cardcolor
                                               && card1.isDraggable
                                               && card2.isDraggable
                                               //&& !t.Contains(card2)
                                               && !card1.isInFreeCell && !card2.isInFreeCell 
                                               && !card1.isInFoundationCell && !card2.isInFoundationCell
                                         select System.Tuple.Create(t, card2))
                                     .FirstOrDefault();
                if(sequenceHintCards!=null)
                {
                            canBreakLoop = true;
                            Debug.Log($"Matching Cards sequence: {sequenceHintCards.Item1}\t {sequenceHintCards.Item2}");
                            foreach (Card c in sequenceHintCards.Item1)
                                Animate(c.transform);
                            Animate(sequenceHintCards.Item2.transform);
                            break;
                            //Check if there is sequence available and if so then check if it's compatible with all table cards ot not
                        }
                }
                }
                else{
                    Debug.Log("Game Over");
                    UIManager.instance.ShowScreen(UIScreens_ENM.resultScreen,false);
                    GameComplete.OnGameComplete(false, intScore, intMoves, timerText.text);
                // Here will be game over condition
                }

            }
            if (canBreakLoop)
                break;
        }
    }

    void Animate(Transform t)
    {
        Debug.Log(t.name + "t.Name");
        t.DOScale(Vector3.one * 1.3f, 0.2f).OnComplete(() => t.DOScale(Vector3.one, 0.2f)).SetLoops(2,LoopType.Yoyo);
    }
}
