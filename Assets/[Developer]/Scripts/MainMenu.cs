using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Level
{
    Easy,
    Medium,
    Hard
}

public class MainMenu : MonoBehaviour
{
    public static Level level=Level.Easy;
    public static string difficultyLevel = "Easy";
    public static bool gameStarted = false;

    public void OnClickVsFriend()
    {
        gameStarted = true;

        Debug.Log($"difficultyLevel {difficultyLevel} \t Enum: {level}");
        UIManager.instance.ShowNextScreenAndHideAllOtherScreen(UIScreens_ENM.gamePlayScreen,false);
        GameManager.instance.counterTxt.SetActive(true);
        GameManager.instance.blockImage.SetActive(false);
    }

    public void OnDrpoDownValueChanged(TextMeshProUGUI tmp)
    {
        difficultyLevel = tmp.text;
        level = Level.Easy.ToString().Equals(difficultyLevel) ? level = Level.Easy : Level.Medium.ToString().Equals(difficultyLevel) ? level = Level.Medium :
            Level.Hard.ToString().Equals(difficultyLevel) ? level = Level.Hard : level = Level.Hard;
    }


    public void OnClickExitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    public void OnClickMainMenu()
    {
        SceneManager.LoadScene(0);        
    }
}
