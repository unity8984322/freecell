using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableCell : MonoBehaviour
{
    public int stackIndex;
    public int childLength;

    public void SetData(int index)
    {
        stackIndex = index;
        childLength = GetLength();
    }

    public int GetLength()
    {
        return transform.childCount+1;
    }
}
